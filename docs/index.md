# Welcome to the DevOps Dojo Documentation Site

[Get the DevOps Dojo CLI](https://devopsdojo.gitlab.io/getdevopsdj/). Members of the DevOps Dojo use a [little helper CLI](./cli/) tool called [devopsdj](./cli/)

Members of the DevOps Dojo regularly practice the following forms:

* [Level 1-1](./level1-1/)
* [Level 1-2](./level1-2/)

[Work on the code for the DevOps Dojo CLI](https://gitlab.com/devopsdojo/devopsdj)
