## What is the devopsdj?

The DevOps Dojo CLI is a little command line helper for members of the Dojo.

The CLI download site is here: [https://devopsdojo.gitlab.io/getdevopsdj/](https://devopsdojo.gitlab.io/getdevopsdj/)

You can install it by running `curl -fL https://devopsdojo.gitlab.io/getdevopsdj/install.sh | bash`
from a Mac or Linux terminal or from a [bash window](https://git-scm.com/) on Windows.
This will install the devopsdj command to your current directory. Feel free to
put it in a place of your choosing that works well in your command line path.

## Commands

* `devopsdj version` - display the current version of the DJ.
* `devopsdj validate` - have the CLI inspect the current directory for for a
dojo level and validate it.
* `devopsdj` - print this help message.
