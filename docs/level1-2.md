# Memorization
`Automate even the simplest repeated tasks`

# Goal
Use a better editor, automate repetitive commands, and commit a few times to a  Git repo.

# Core skills
| Bash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| Atom | Git |
|---|---|---|

# Form

## Atom
1. Download and install [Atom](https://atom.io)
1. Add the autocomplete-bash-builtin package to Atom (Install in Atom's preferences)
1. Add the platform-ide-terminal package to Atom
1. Create a new directory on your computer in a place of your choosing (use a terminal!)
1. Add the directory as a Project Folder in Atom

## Bash and Git
1. Create a new file in your project directory in Atom called `build.sh` (Right
  click on project directory > New File).
1. Add `#!/bin/bash` to the top of the file and `set -e` on the second line.
  These two lines tell the terminal that we are using bash for our language and
  that we should quit if there is an error in the run.
1. Add the commands to build.sh to create a directory in the project directory
  called `dist` and echo the text `'phase 1'` to a file called `./dist/level1-2.txt`
  (`mkdir -p ./dist` and `echo 'phase 1' > ./dist/level1-2.txt`)
1. Save the build.sh file in Atom (File > Save)
1. Open the Atom terminal you installed (`ctrl-backtick`)
1. Change the mode of the build.sh file to be executable (`chmod +x ./build.sh`)
1. Test run your build.sh file, verify it creates the file in ./dist (`./build.sh`)
1. Add a .gitignore file with `dist/` in it to ignore the dist directory
1. Add and commit your changes to Git
1. Set and environment variable named `LEVEL_1_2_TEXT` with the value `phase 2`
  (`export LEVEL_1_2_TEXT="phase 2"`)
1. Change build.sh to use an environment variable instead of a literal 'phase 1'
  string (change `echo 'phase 1' > ./dist/level1-2.txt` to
  `echo $LEVEL_1_2_TEXT > ./dist/level1-2.txt`)
1. Test run your build.sh file, verify it changes the file in ./dist (`./build.sh`)
1. Add `devopsdj validate` to the bottom of the build script.
1. Run the build file again, devopsdj should validate successfully.
1. Add and commit your changes to Git

Your build.sh file should look like this
```
#!/bin/bash
set -e

mkdir -p ./dist  # the -p keeps mkdir from returning non-zero if dir already exists
echo $LEVEL_1_2_TEXT > ./dist/level1-2.txt
```
