# Memorization
`DevOps starts on the command line`

# Goal
Acquire basic command line skills, perform simple file editing, and turn a directory into a Git repo.

# Core skills
| Bash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| vim&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| Git |
|---|---|---|

# Form

## Bash
1. Navigate to system temp directory at a Bash prompt
1. Clear the terminal
1. Create a directory
1. List all files in long format
1. Change to directory
1. Echo "hello 1" into level1_1.txt file
1. View file contents
1. Echo "hello 2" into level1_2.txt file
1. Amend level1_1.txt with contents of level1_2.txt
1. Make level1_2.txt read only and prove it
1. Grep hosts file for 'localhost'
1. Find all files named '\*.log'
1. Install [DevOps Dojo CLI](https://devopsdojo.gitlab.io/getdevopsdj/)

## vim
1. Make level1-1.txt writeable for only you, read only for all
1. Open file level1-1.txt in vi
1. Delete the whole line that has "hello 2"
1. Insert text a new line of text at the top and type "hello 3"
1. Delete the "ll" out "hello 1"
1. Find some text
1. Save the file
1. Quit

## Git
1. Configure Git for the directory you created
1. Show status
1. Add a .gitignore file and ignore level1-2.txt
1. Commit changes
1. Show status again
1. Show log
1. Verify your form by running `devopsdj validate`
